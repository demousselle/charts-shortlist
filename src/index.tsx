import './base.scss'

import { StrictMode } from 'react'
import ReactDOM from 'react-dom'
import TagManager from 'react-gtm-module'
import { BrowserRouter } from 'react-router-dom'
import { AuthProvider } from '@matillion/hub-client'
import { datadogLogs } from '@datadog/browser-logs'
import { datadogRum } from '@datadog/browser-rum'

import config from './config'
import App from './App'

if (config.datadog) {
  const { rumService, service, apiOrigin, ...options } = config.datadog

  datadogLogs.init({
    ...options,
    service,
    forwardErrorsToLogs: true,
    sampleRate: 100
  })

  datadogRum.init({
    ...options,
    version: config.version,
    service: rumService,
    sampleRate: 100,
    allowedTracingOrigins: [apiOrigin]
  })
}

if (config.gtmId) {
  TagManager.initialize({ gtmId: config.gtmId })
}

const root = document.getElementById('root')

ReactDOM.render(
  <StrictMode>
    <AuthProvider environment={config.authEnvironment}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </AuthProvider>
  </StrictMode>,
  root,
  () => {
    root?.setAttribute('data-app-environment', config.environment)
    root?.setAttribute('data-app-version', config.version)
  }
)
