import { Matchers } from '@pact-foundation/pact'
import { pactWith } from 'jest-pact'

import { getCurrentOrganisation } from './hub'
import packageInfo from '../../package.json'

const { string, uuid } = Matchers

const responseWith = (body: Record<string, unknown>, statusCode: number) => ({
  status: statusCode,
  headers: {
    'Content-Type': 'application/json'
  },
  body: body
})

const currentOrganisationRequest = {
  uponReceiving: "A request for the user's current organisation",
  withRequest: {
    method: 'GET' as const,
    path: '/hub/v1/organisations/current'
  }
}

pactWith(
  {
    consumer: packageInfo.name,
    provider: 'hub-platform',
    pactfileWriteMode: 'overwrite',
    cors: true
  },
  (provider) => {
    describe('Get current organisation', () => {
      beforeEach(async () => {
        await provider.addInteraction({
          state: 'Organisation Exists',
          ...currentOrganisationRequest,
          willRespondWith: responseWith(
            {
              id: uuid('ebe2f973-579f-4d40-62c6-555ad14a6132'),
              name: string('Test Org'),
              userRoles: ['billing:admin', 'hub:admin', 'mdl:admin']
            },
            200
          )
        })
      })

      it('should return your current organisation information', async () => {
        const { data } = await getCurrentOrganisation('', {
          baseURL: `${provider.mockService.baseUrl}/hub`
        })

        expect(data).toStrictEqual({
          id: 'ebe2f973-579f-4d40-62c6-555ad14a6132',
          name: 'Test Org',
          userRoles: ['billing:admin', 'hub:admin', 'mdl:admin']
        })
      })
    })
  }
)
