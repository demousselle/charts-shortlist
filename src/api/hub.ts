import axios, { AxiosRequestConfig } from 'axios'

const hubApi = axios.create({
  headers: { 'Content-Type': 'application/json' }
})

interface CurrentOrganisationResponse {
  id: string
  subdomain: string
  name: string
  userRoles: string[]
}

/**
  You won't need to call this your application, as the Hub
  Client's AuthProvider will load the current user information
  from this endpoint for you on load. It's added here as an example
  of how we favour making calls in our applications.

  This is used in `hub.pact.spec.ts` to do contract testing against the
  Hub, so we would recommend leaving it in your codebase! Removing this
  will mean that any changes to this endpoint on the Hub could
  potentially break your application in the future.
*/
export const getCurrentOrganisation = async (
  token: string,
  requestConfig?: AxiosRequestConfig
) =>
  hubApi.get<CurrentOrganisationResponse>('/v1/organisations/current', {
    ...requestConfig,
    headers: { authorization: `Bearer ${token}` }
  })

export default hubApi
