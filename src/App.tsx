import { FunctionComponent } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { useAuth } from '@matillion/hub-client'

import AppLayout from './components/AppLayout'
import { Login, Logout } from './modules/Auth'
import Dashboard from './modules/Dashboard'

const App: FunctionComponent = () => {
  const { isLoggedIn } = useAuth()

  if (!isLoggedIn) {
    return <Login />
  }

  return (
    <AppLayout>
      <Switch>
        <Route exact path="/logout">
          <Logout />
        </Route>

        <Route exact path="/dashboard">
          <Dashboard />
        </Route>

        <Redirect to="/dashboard" />
      </Switch>
    </AppLayout>
  )
}

export default App
