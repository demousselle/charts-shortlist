import { FunctionComponent } from 'react'
import { useUser } from '@matillion/hub-client'
import {
  Container,
  BannerContainer,
  Section,
  Typography
} from '@matillion/component-library'

import classes from './Dashboard.module.scss'

const Dashboard: FunctionComponent = () => {
  const { user } = useUser()

  return (
    <>
      <BannerContainer>
        <Section className={classes.Dashboard__Banner}>
          <Typography format="dtl">Hello, {user.given_name}!</Typography>
        </Section>
      </BannerContainer>

      <Container>
        <Section>
          <Typography format="tm">Welcome to your new app!</Typography>
        </Section>

        <Section>
          <Typography format="bcm">💚</Typography>
        </Section>
      </Container>
    </>
  )
}

export default Dashboard
