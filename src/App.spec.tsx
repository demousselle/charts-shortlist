import { render } from './__test-utils__'
import { useAuth, useUser } from '@matillion/hub-client'

import App from './App'

jest.mock('@matillion/hub-client')
const mockUseAuth = useAuth as jest.Mock<ReturnType<typeof useAuth>>
const mockUseUser = useUser as jest.Mock<ReturnType<typeof useUser>>

describe('The application', () => {
  describe('When logged in', () => {
    beforeEach(() => {
      mockUseAuth.mockReturnValue({
        isLoading: false,
        isLoggedIn: true,
        login: jest.fn(),
        logout: jest.fn(),
        getToken: jest.fn()
      })

      mockUseUser.mockReturnValue({
        user: {
          given_name: 'Jeffrey',
          email: 'Jeff@rey.com',
          name: 'Jeffrey McElroy',
          picture: 'https://placekitten.com/200/200',
          nickname: 'jeffrey.mcelroy',
          updated_at: '2021-06-15T13:21:30.395Z'
        },
        organisation: {
          id: '00000000-0000-0000-0000-000000000000',
          name: 'Munch Squad'
        },
        profile: {
          email: 'jeff@rey.com',
          name: 'Jeffrey McElroy',
          icon: 'https://placekitten.com/200/200',
          organisation: 'Munch Squad'
        },
        roles: new Set(['hub:admin'])
      })
    })

    it('should render a dashboard', () => {
      const { getByText, asFragment } = render(<App />)

      expect(getByText('Hello, Jeffrey!')).toBeInTheDocument()
      expect(asFragment()).toMatchSnapshot()
    })
  })

  describe('When logged out', () => {
    it('should redirect the user to the login screen', () => {
      const mockLogin = jest.fn()
      mockUseAuth.mockReturnValue({
        isLoading: false,
        isLoggedIn: false,
        login: mockLogin,
        logout: jest.fn(),
        getToken: jest.fn()
      })

      render(<App />)

      expect(mockLogin).toHaveBeenCalled()
    })
  })
})
