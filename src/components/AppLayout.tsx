import { FunctionComponent } from 'react'
import { Link } from 'react-router-dom'
import { useUser, SwitchOrganisationLink } from '@matillion/hub-client'
import {
  Header,
  Layout,
  UserMenu,
  UserMenuItemGroup,
  UserMenuItem
} from '@matillion/component-library'

const Menu: FunctionComponent = () => {
  const { profile } = useUser()

  return (
    <UserMenu profile={profile}>
      <UserMenuItemGroup>
        <UserMenuItem>
          <SwitchOrganisationLink>Switch Organization</SwitchOrganisationLink>
        </UserMenuItem>
        <UserMenuItem>
          <Link to="/logout">Logout</Link>
        </UserMenuItem>
      </UserMenuItemGroup>
    </UserMenu>
  )
}

const AppHeader: FunctionComponent = () => <Header menu={<Menu />} />

const AppLayout: FunctionComponent = ({ children }) => (
  <Layout header={<AppHeader />}>{children}</Layout>
)

export default AppLayout
