import '@testing-library/jest-dom'

import { FunctionComponent, ReactElement } from 'react'
import { render, RenderOptions } from '@testing-library/react'
import { Router } from 'react-router-dom'
import { createMemoryHistory, MemoryHistory } from 'history'

interface CustomRenderOptions extends RenderOptions {
  history?: MemoryHistory
  url?: string
}

const customRender = (
  ui: ReactElement,
  {
    history = createMemoryHistory(),
    url,
    ...renderOptions
  }: CustomRenderOptions = {}
): ReturnType<typeof render> => {
  if (url !== undefined) {
    history.push(url)
  }

  const Wrapper: FunctionComponent = ({ children }) => {
    return <Router history={history}>{children}</Router>
  }

  return render(ui, { wrapper: Wrapper, ...renderOptions })
}

export * from '@testing-library/react'

export { customRender as render }
