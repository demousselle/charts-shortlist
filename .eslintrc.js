const config = require('@matillion/frontend-shared-config/eslint')

module.exports = {
  ...config,

  parserOptions: {
    ...config.parserOptions,
    project: ['./tsconfig.eslint.json']
  }
}
