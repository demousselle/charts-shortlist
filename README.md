# Matillion React Application

This project was created with [Create React App](https://github.com/facebook/create-react-app) and the [Matillion front end application template](https://bitbucket.org/matillion/cra-template-matillion/).

## Running the app locally

Most Matillion applications assume that the URL at which they're accessed follows the `<organisation>.<application>.matillion.com` format--we know what organisation the user is currently accessing by reading that organisation subdomain! In order to run your application locally, you'll need to add an entry to your hosts file to create a mock domain to run your application against. You can do this using the following command:

```
echo "127.0.0.1 <myorg>.<myapp>.matillion.com" | sudo tee -a /etc/hosts
```

You can then run your application locally:

```
HTTPS=true PORT=3000 HOST=<myorg>.<myapp>.matillion.com npm run start
```

## Linting

We use Husky and lint-staged to make sure your code is free of errors and is properly formatted before commit. When you make a commit, `eslint --fix`, `stylelint --fix` and `prettier --write` will be run on any included files, which should ensure everything is in the correct format. We'd recommend having your editor format and lint files on save as well, though!

You can also check your code style manually, using the `npm run lint` command. This will also be run in the pipeline!

## Running your tests

The application's unit tests can be run with the `npm run test:unit` command. This will open Jest in watch mode, by default,

You can run the Cypress tests in one of two ways: you can use the `npm run cypress:local:run` command to run them in the console, or use the `npm run cypress:local:open` command to open Cypress' interactive UI.

## Building the app

The application can be built by running `npm run build`, which will create a `build` folder containing your bundled code.

## Deploying the app

See [Using Vault from CI/CD for Deployment Secrets](https://matillion.atlassian.net/wiki/spaces/AIT/pages/2497347616) for details on how add a new Bitbucket pipeline for this new project.

In Bitbucket, rename the default deployment environments as follows:
- **Test** to **Dev**
- **Staging** to **Preprod**
- **Production** to **Prod**
