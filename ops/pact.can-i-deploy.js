const pact = require('@pact-foundation/pact-node')
const packageInfo = require('../package.json')

const consumerTag = process.env.BITBUCKET_BRANCH

const opts = {
  pacticipants: [{ name: packageInfo.name, latest: consumerTag }],
  pactBroker: process.env.PACT_BROKER_BASE_URL,
  pactBrokerUsername: process.env.PACT_BROKER_USERNAME,
  pactBrokerPassword: process.env.PACT_BROKER_PASSWORD,
  output: 'table',
  retryWhileUnknown: 5,
  retryInterval: 5
}

const canIDeploy = async () => {
  try {
    const result = await pact.canDeploy(opts)
    console.log(result)
  } catch (e) {
    console.error(e)
    process.exit(1)
  }
}

canIDeploy()
