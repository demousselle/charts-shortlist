const semanticRelease = require('semantic-release')
const childProcess = require('child_process')
const packageInfo = require('../package.json')
const releaseConfig = require('../.releaserc.js')
const fs = require('fs')

const OUTPUT_FILE = '.BUILD_VARIABLES'

const DEFAULT_VERSION = `${packageInfo.version}-rc.${
  process.env.BITBUCKET_PR_ID || 0
}`
const BUILD_TIME = new Date()
const BUILD_TIMESTAMP = `${BUILD_TIME.toLocaleDateString()}-${BUILD_TIME.toLocaleTimeString()}`
const UNIQUE_RUN_ID = `${process.env.BITBUCKET_PIPELINE_UUID}-${BUILD_TIMESTAMP}`
const COMMIT_HASH = childProcess
  .execSync('git rev-parse HEAD')
  .toString()
  .trim()

const BUILD_VARS = {
  COMMIT_HASH,
  UNIQUE_RUN_ID,
  BUILD_VERSION: DEFAULT_VERSION,
  CYPRESS_BUILD_ID: UNIQUE_RUN_ID
}

semanticRelease({ ...releaseConfig, ci: false, dryRun: true })
  .then((result) => {
    if (!result) {
      return
    }

    BUILD_VARS.BUILD_VERSION = result.nextRelease.version
  })
  .catch((e) => {
    console.error('Semantic release failed with %O', e)
  })
  .then(() => {
    const output = fs.createWriteStream(OUTPUT_FILE)

    Object.entries(BUILD_VARS)
      .filter(([, value]) => !!value)
      .forEach(([name, value]) => {
        output.write(`${name}=${value}\n`)
      })

    output.close()
  })
