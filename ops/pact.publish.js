const pact = require('@pact-foundation/pact-node')
const path = require('path')

const tag = process.env.BITBUCKET_BRANCH
const version =
  process.env.BITBUCKET_BRANCH === 'main'
    ? process.env.BUILD_VERSION
    : process.env.BITBUCKET_BUILD_NUMBER

const opts = {
  pactFilesOrDirs: [path.resolve(__dirname, '../pact/pacts')],
  pactBroker: process.env.PACT_BROKER_BASE_URL,
  pactBrokerUsername: process.env.PACT_BROKER_USERNAME,
  pactBrokerPassword: process.env.PACT_BROKER_PASSWORD,
  consumerVersion: version,
  tags: [tag]
}

const publishPacts = async () => {
  if (!process.env.CI) {
    console.log('skipping Pact publish...')
    return
  }

  try {
    await pact.publishPacts(opts)
    console.log('Pact contract publishing complete!')
  } catch (e) {
    console.log('Pact contract publishing failed: ', e)
  }
}

publishPacts()
