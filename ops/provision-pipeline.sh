#!/bin/bash

# Set Error
set -e

STS_FILE="sts.json"

# Login to Vault and set client_token value as VAULT_TOKEN
VAULT_TOKEN=$(curl --silent -d '{"role_id":"'${VAULT_APPROLE_ID}'","secret_id":"'${VAULT_APPROLE_SECRET_ID}'"}' -X POST ${VAULT_ADDR}/v1/auth/approle_pipelines/login |  jq -r '.auth.client_token')

if [ -z "${VAULT_TOKEN}" ]; then
  echo "Error: Don't get a Vault Token!"
  exit 1
fi

# Get STS creds for AWS CodeArtifact
curl --silent -H "X-Vault-Token: ${VAULT_TOKEN}" -X GET ${VAULT_ADDR}/v1/aws_cactus_test/sts/codeartifact-access-role | jq '.' > $STS_FILE

# AWS credential Env Vars
export AWS_ACCESS_KEY_ID=$(jq -r '.data.access_key' $STS_FILE)
export AWS_SECRET_ACCESS_KEY=$(jq -r '.data.secret_key' $STS_FILE)
export AWS_SESSION_TOKEN=$(jq -r '.data.security_token' $STS_FILE)
export AWS_DEFAULT_REGION=$AWS_REGION

# Login to CodeArtifact component library
/root/bin/aws codeartifact login --tool npm \
  --repository ${CODEARTIFACT_REPO:-matillion-frontend} \
  --domain ${CODEARTIFACT_DOMAIN:-matillion-frontend} \
  --domain-owner ${CODEARTIFACT_AWS_ACCOUNT_ID:-458549032908} \
  --region ${CODEARTIFACT_REGION:-eu-west-1}

# Get the deployment AWS creds
curl --silent -H "X-Vault-Token: ${VAULT_TOKEN}" -X GET ${VAULT_ADDR}/v1/${PIPELINE_CSP_ACCOUNT}/sts/${PIPELINE_CSP_DEPLOYMENT_ROLE} | jq '.' > $STS_FILE
export AWS_ACCESS_KEY_ID=$(jq -r '.data.access_key' $STS_FILE)
export AWS_SECRET_ACCESS_KEY=$(jq -r '.data.secret_key' $STS_FILE)
export AWS_SESSION_TOKEN=$(jq -r '.data.security_token' $STS_FILE)
export AWS_DEFAULT_REGION=$AWS_REGION
