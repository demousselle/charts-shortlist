const {
  appReleaseConfig
} = require('@matillion/frontend-shared-config/semantic-release')

module.exports = appReleaseConfig
