Cypress.Commands.add('setHubProfile', (mockProfile, mockAuthState) => {
  Cypress.on('window:before:load', (clientWindow) => {
    clientWindow.AuthProvider = {
      mockProfile,
      mockAuthState
    }
  })
})
