/// <reference types="cypress" />
/// <reference types="@matillion/hub-client" />

import Dashboard from '../pages/Dashboard'

describe('Dashboard', () => {
  const dashboard = new Dashboard()

  it('should show the user a welcome message', () => {
    cy.setHubProfile({
      given_name: 'Test'
    })

    dashboard.visit()
    dashboard.welcomeBanner('Test').should('be.visible')
  })
})
