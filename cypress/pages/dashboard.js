/// <reference types="cypress" />

export default class Dashboard {
  visit() {
    cy.visit('/dashboard')
    cy.contains('Welcome to your new app!', { timeout: 20000 })
  }

  welcomeBanner(name) {
    return cy.contains(`Hello, ${name}!`)
  }
}
