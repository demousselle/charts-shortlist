module.exports = {
  '*.{ts,tsx}': 'eslint --fix',
  '*.{css,scss}': 'stylelint --fix',
  '*.{ts,tsx,css,scss,md,json,yml}': 'prettier --write'
}
